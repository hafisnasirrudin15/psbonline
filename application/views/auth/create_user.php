<h1><?php echo lang('create_user_heading');?></h1>
<p><?php echo lang('create_user_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/create_user");?>

      <p>
            <?php echo lang('create_user_nama_ananda_label', 'nama_ananda');?> <br />
            <?php echo form_input($nama_ananda);?>
      </p>

      
      <p>
            <?php echo lang('create_user_nik_ananda_label', 'nik_ananda');?> <br />
            <?php echo form_input($nik_ananda);?>
      </p>

      <p>
            <?php echo lang('create_user_nama_ortu_label', 'nama_ortu');?> <br />
            <?php echo form_input($nama_ortu);?>
      </p>

      
      <p>
            <?php echo lang('create_user_nik_ortu_label', 'nik_ortu');?> <br />
            <?php echo form_input($nik_ortu);?>
      </p>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

      <p>
            <?php echo lang('create_user_asal_sekolah_label', 'asal_sekolah');?> <br />
            <?php echo form_input($asal_sekolah);?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn'));?></p>

<?php echo form_close();?>
